require 'test_helper'

class PostTest < ActiveSupport::TestCase
    test "should not save post without title or body" do
        post = Post.new
        assert_not post.save
        post.title = 'Test'
        assert_not post.save
        post.body = 'Test body'
        assert post.save
    end
    test "should have the necessary required validators" do
        post = Post.new
        assert_not post.valid?
        assert_equal [:title, :body], post.errors.keys
    end
    # test "the truth" do
  #   assert true
  # end
end
